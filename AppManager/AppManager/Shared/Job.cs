﻿namespace AppManager.Shared
{
    public class Job
    {
        public string Name { get; set; }

        public Script Script { get; set; }

        public static bool operator ==(Job j1, Job j2)
        {
            if (ReferenceEquals(j1, j2))
            {
                return true;
            }

            if (j1 is null || j2 is null)
            {
                return false;
            }

            return CheckEquality(j1, j2);
        }

        public static bool operator !=(Job j1, Job j2) => !(j1 == j2);

        public override bool Equals(object other) => this == other as Job;

        public bool Equals(Job other) => other == this;

        public override int GetHashCode() => (Name, Script).GetHashCode();

        private static bool CheckEquality(Job j1, Job j2)
        {
            return j1.Name == j2.Name && j1.Script == j2.Script;
        }
    }
}
