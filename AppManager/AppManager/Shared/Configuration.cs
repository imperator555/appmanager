﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AppManager.Shared
{
    public class Configuration : IEquatable<Configuration>
    {
        public IEnumerable<Environment> Environments { get; set; }

        public Configuration()
        {
            Environments = new List<Environment>();
        }

        public Process FindProcess(string environment, string process)
        {
            return FindEnvironment(environment)?.FindProcess(process);
        }

        public Environment FindEnvironment(string environment)
        {
            return Environments?.Where(env => env.Name == environment).FirstOrDefault();
        }

        public ProcessGroup FindGroup(string environment, string group)
        {
            return FindEnvironment(environment)?.FindProcessGroup(group);
        }

        public Job FindJob(string environment, string job)
        {
            return FindEnvironment(environment).FindJob(job);
        }

        public IEnumerable<string> GetGlobalScriptsForEnvironment(string environmentName, ScriptType scriptType)
        {
            return FindEnvironment(environmentName).StartupScripts
                .Where(script => script.Type == scriptType)
                .Select(script => script.Path)
                .ToList();
        }

        public static bool operator ==(Configuration c1, Configuration c2)
        {
            if (ReferenceEquals(c1, c2))
            {
                return true;
            }

            if (c1 is null || c2 is null || c1.Environments.Count() != c2.Environments.Count())
            {
                return false;
            }

            foreach (var env1 in c1.Environments)
            {
                var env2 = c2.FindEnvironment(env1.Name);

                if (env1 != env2)
                {
                    return false;
                }
            }

            return true;
        }

        public static bool operator !=(Configuration c1, Configuration c2) => !(c1 == c2);

        public override bool Equals(object other) => (other as Configuration) == this;

        public override int GetHashCode() => Environments.GetHashCode();

        public bool Equals(Configuration other) => this == other;
    }
}
