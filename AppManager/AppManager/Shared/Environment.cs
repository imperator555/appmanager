﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AppManager.Shared
{
    public class Environment : IEquatable<Environment>
    {
        public string Name { get; set; }

        public IEnumerable<Process> IndividualProcesses { get; set; }

        public IEnumerable<Job> Jobs { get; set; }

        public IEnumerable<ProcessGroup> Groups { get; set; }

        public IEnumerable<Script> StartupScripts { get; set; }

        public Environment()
        {
            IndividualProcesses = new List<Process>();
            Groups = new List<ProcessGroup>();
            Jobs = new List<Job>();
            StartupScripts = new List<Script>();
        }

        public static bool operator ==(Environment e1, Environment e2)
        {
            if (ReferenceEquals(e1, e2))
            {
                return true;
            }

            if (e1 is null || e2 is null)
            {
                return false;
            }

            return CheckEquality(e1, e2);
        }

        public static bool operator !=(Environment e1, Environment e2) => !(e1 == e2);

        public override bool Equals(object other) => this == other as Environment;

        public override int GetHashCode() => (Name, IndividualProcesses, Groups, Jobs).GetHashCode();

        public bool Equals(Environment other) => this == other;

        internal Process FindProcess(string process)
        {
            return IndividualProcesses.Where(proc => proc.Name == process).FirstOrDefault();
        }

        internal ProcessGroup FindProcessGroup(string group)
        {
            return Groups.Where(grp => grp.Name == group).FirstOrDefault();
        }

        internal Job FindJob(string job)
        {
            return Jobs.Where(j => j.Name == job).FirstOrDefault();
        }

        private static bool CheckEquality(Environment e1, Environment e2)
        {
            if (e1.Name != e2.Name || e1.IndividualProcesses.Count() != e2.IndividualProcesses.Count() 
                || e1.Groups.Count() != e2.Groups.Count() || e1.Jobs.Count() != e2.Jobs.Count())
            {
                return false;
            }

            foreach (var proc1 in e1.IndividualProcesses)
            {
                var proc2 = e2.FindProcess(proc1.Name);

                if (proc1 != proc2)
                {
                    return false;
                }
            }

            foreach (var group1 in e1.Groups)
            {
                var group2 = e2.FindProcessGroup(group1.Name);

                if (group1 != group2)
                {
                    return false;
                }
            }

            foreach (var job1 in e1.Jobs)
            {
                var job2 = e2.FindJob(job1.Name);

                if (job1 != job2)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
