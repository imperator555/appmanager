﻿namespace AppManager.Shared
{
    public class Script
    {
        public string Path { get; set; }

        public ScriptType Type { get; set; }
    }
}
