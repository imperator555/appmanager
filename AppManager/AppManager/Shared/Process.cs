﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AppManager.Shared
{
    public class Process : IEquatable<Process>
    {
        public string Name { get; set; }

        public ProcessHealth Health { get; set; }

        public Script HealthScript { get; set; }

        public Script StartScript { get; set; }

        public Script StopScript { get; set; }

        public IEnumerable<string> Dependencies { get; set; }

        public Process()
        {
            Dependencies = new List<string>();
        }

        public static bool operator ==(Process p1, Process p2)
        {
            if (ReferenceEquals(p1, p2))
            {
                return true;
            }

            if (p1 is null || p2 is null)
            {
                return false;
            }

            return CheckEquality(p1, p2);
        }

        public static bool operator !=(Process p1, Process p2) => !(p1 == p2);

        public override bool Equals(object other) => this == other as Process;

        public bool Equals(Process other) => other == this;

        public override int GetHashCode() => (Name, Health).GetHashCode();

        private static bool CheckEquality(Process p1, Process p2)
        {
            if (p1.Name != p2.Name || p1.Health != p2.Health)
            {
                return false;
            }

            if (p1.Dependencies.Count() != p2.Dependencies.Count())
            {
                return false;
            }

            foreach (var dep1 in p1.Dependencies)
            {
                if (!p2.Dependencies.Any(dep2 => dep1 == dep2))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
