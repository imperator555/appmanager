﻿using System.Collections.Generic;
using System.Linq;

namespace AppManager.Shared
{
    public class ProcessGroup
    {
        public string Name { get; set; }
        public IEnumerable<Process> Processes { get; set; }

        public static bool operator ==(ProcessGroup pg1, ProcessGroup pg2)
        {
            if (ReferenceEquals(pg1, pg2))
            {
                return true;
            }

            if (pg1 is null || pg2 is null)
            {
                return false;
            }

            return CheckEquality(pg1, pg2);
        }

        public static bool operator !=(ProcessGroup pg1, ProcessGroup pg2) => !(pg1 == pg2);

        public override bool Equals(object other) => this == other as ProcessGroup;

        public bool Equals(ProcessGroup other) => other == this;

        public override int GetHashCode() => (Name, Processes).GetHashCode();

        private static bool CheckEquality(ProcessGroup pg1, ProcessGroup pg2)
        {
            if (pg1.Name != pg2.Name)
            {
                return false;
            }

            foreach (var proc1 in pg1.Processes)
            {
                var proc2 = pg2.Processes.Where(p => p.Name == proc1.Name).FirstOrDefault();

                if (proc1 != proc2)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
