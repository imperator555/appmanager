﻿using System;

namespace AppManager.Shared
{
    public interface ISimpleDisposable : IDisposable
    {
        protected bool Disposed { get; set; }

        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        protected void OnDisposing();

        private void Dispose(bool disposing)
        {
            if (!Disposed)
            {
                if (disposing)
                {
                    OnDisposing();
                }

                Disposed = true;
            }
        }
    }
}
