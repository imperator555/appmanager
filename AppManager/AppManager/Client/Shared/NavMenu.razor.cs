﻿using AppManager.Shared;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading;
using System.Threading.Tasks;

namespace AppManager.Client.Shared
{
    public partial class NavMenu : ComponentBase, ISimpleDisposable
    {
        private IEnumerable<string> environments;
        private bool collapseNavMenu = true;
        private Timer serverHealthCheckTimer;

        private string NavMenuCssClass => collapseNavMenu ? "collapse" : null;

        private string HealthCssClass { get; set; } = "healthy";

        bool ISimpleDisposable.Disposed { get; set; }

        [Inject]
        private HttpClient Http { get; set; }

        void ISimpleDisposable.OnDisposing()
        {
            serverHealthCheckTimer.Dispose();
        }

        protected override async Task OnInitializedAsync()
        {
            environments = await Http.GetFromJsonAsync<IEnumerable<string>>("Process/environments");
            StartServerHealthCheckTimer();
        }

        private void ToggleNavMenu()
        {
            collapseNavMenu = !collapseNavMenu;
        }

        private string CreateManagerLink(string env)
        {
            return $"manager/{env}";
        }

        private void StartServerHealthCheckTimer()
        {
            serverHealthCheckTimer = new Timer(OnServerHealthCheckWrapper, null, 0, 5000);
        }

        private async void OnServerHealthCheckWrapper(object state)
        {
            // To catch exceptions when calling async methods the method must return a Task, however the Timer
            // callback expects a void method. So we use an async void method as a wrapper and put logic in an
            // async Task method.
            await OnServerHealthCheck();
        }

        private async Task OnServerHealthCheck()
        {
            string result = string.Empty;

            try
            {
                result = await Http.GetStringAsync("health");
            } 
            catch (Exception e)
            {
                // check if server is down otherwise rethrow the exception
                if (!e.Message.Contains("NetworkError when attempting to fetch resource"))
                {
                    throw;
                }
            }

            var nextClass = result == "Healthy" ? "healthy" : "unhealthy";
            var needRefresh = nextClass != HealthCssClass;

            if (needRefresh)
            {
                HealthCssClass = nextClass;
                StateHasChanged();
            }
        }
    }
}
