﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading;
using System.Threading.Tasks;
using AppManager.Client.Helpers;
using AppManager.Shared;
using Microsoft.AspNetCore.Components;

namespace AppManager.Client.Pages
{
    public partial class Manager : ComponentBase, ISimpleDisposable
    {
        private const int PollingPeriod = 10000;
        private const int AnimationUpdatePeriod = 400;

        private Model.Configuration configuration;
        private IEnumerable<Model.Process> processes;
        private Model.Environment environment;
        private IEnumerable<(string, IEnumerable<Model.Process>)> groups;
        private string timerCircleStyle = "stroke-dasharray: 364 364; stroke-dashoffset: 364";
        private Timer pollingTimer;
        private int pollCounter = PollingTick;
        private bool isPageVisible = true;
        private bool isCurrentTabRequiresPolling = true;

        [Parameter]
        public string EnvironmentName { get; set; }

        [Inject] 
        private HttpClient Http { get; set; }

        [Inject] 
        private Blazored.LocalStorage.ILocalStorageService LocalStorage { get; set; }

        [Inject] 
        private Services.IMapperService Mapper { get; set; }

        bool ISimpleDisposable.Disposed { get; set; }

        private static int PollingTick => (PollingPeriod / AnimationUpdatePeriod) + 1;

        private bool IsPollingRequired => isPageVisible && isCurrentTabRequiresPolling;

        void ISimpleDisposable.OnDisposing()
        {
            pollingTimer.Dispose();
            VisibilityChangedHelper.VisibilityChanged -= OnPageVisibilityChanged;
        }

        protected override void OnInitialized()
        {
            VisibilityChangedHelper.VisibilityChanged += OnPageVisibilityChanged;

            StartPollingTimer();
        }

        private async void StartProcess(string proc)
        {
            await Http.GetAsync($"Process/startProcess?env={EnvironmentName}&proc={proc}");
        }

        private async void StopProcess(string proc)
        {
            await Http.GetAsync($"Process/stopProcess?env={EnvironmentName}&proc={proc}");
        }

        private async void StartEnvironment()
        {
            var exclusions = GetStartExcludedProcessesForEnvironment();
            var exclString = exclusions.Any() ? $"&exclusions={string.Join(',', exclusions)}" : "";
            await Http.GetAsync($"Process/startEnvironment?env={EnvironmentName}{exclString}");
        }

        private async void StopEnvironment()
        {
            var exclusions = GetStopExcludedProcessesForEnvironment();
            var exclString = exclusions.Any() ? $"&exclusions={string.Join(',', exclusions)}" : "";
            await Http.GetAsync($"Process/stopEnvironment?env={EnvironmentName}{exclString}");
        }

        private async void StartGroup(string group)
        {
            var exclusions = GetStartExcludedProcessesForGroup(group);
            var exclString = exclusions.Any() ? $"&exclusions={string.Join(',', exclusions)}" : "";
            await Http.GetAsync($"Process/startGroup?env={EnvironmentName}&group={group}{exclString}");
        }

        private async void StopGroup(string group)
        {
            var exclusions = GetStopExcludedProcessesForGroup(group);
            var exclString = exclusions.Any() ? $"&exclusions={string.Join(',', exclusions)}" : "";
            await Http.GetAsync($"Process/stopGroup?env={EnvironmentName}&group={group}{exclString}");
        }

        private async void RunJob(string job)
        {
            await Http.GetAsync($"Process/runJob?env={EnvironmentName}&job={job}");
        }

        private void StartPollingTimer()
        {
            pollingTimer = new Timer(OnPollingTimerFired, null, 0, AnimationUpdatePeriod);
        }

        private void PausePollingTimer()
        {
            pollingTimer.Change(int.MaxValue, int.MaxValue);
        }

        private void ContinuePollingTimer()
        {
            pollingTimer.Change(AnimationUpdatePeriod, AnimationUpdatePeriod);
        }

        private async void OnPollingTimerFired(object state)
        {
            if (IsPollingRequired)
            {
                if (pollCounter >= PollingTick)
                {
                    PausePollingTimer();
                    await QueryHealtStatus();
                    ContinuePollingTimer();
                    pollCounter = -1;
                }

                UpdatePollingCircleAnimation(pollCounter, PollingPeriod);
            }

            pollCounter++;

            StateHasChanged();
        }

        private async Task QueryHealtStatus()
        {
            try
            {
                var tmp = Mapper.MapConfiguration(await Http.GetFromJsonAsync<AppManager.Shared.Configuration>("Process/health"));
                configuration = await KeepExclusionsInClientConfig(tmp);
                environment = configuration.FindEnvironment(EnvironmentName);
                processes = environment.IndividualProcesses;
                groups = environment.Groups.Select(g => (g.Name, g.Processes)).ToList();
            }
            catch(Exception e)
            {
                // check if server is down otherwise rethrow the exception
                if (!e.Message.Contains("NetworkError when attempting to fetch resource"))
                {
                    throw;
                }
            }
        }

        private void UpdatePollingCircleAnimation(int tick, int interval)
        {
            var circleRadius = 14;
            var circumference = circleRadius * 2 * Math.PI;
            var progress = circumference - Math.Min((double)(interval - tick * AnimationUpdatePeriod) / interval, 1) * circumference;

            timerCircleStyle = $"stroke-dasharray: {circumference} {circumference}; stroke-dashoffset: {progress}";
        }

        private async Task<Model.Configuration> KeepExclusionsInClientConfig(Model.Configuration serverConfiguration)
        {
            var config = await LocalStorage.GetItemAsync<Model.Configuration>("config");
            if (config != null)
            {
                foreach (var serverEnv in serverConfiguration.Environments)
                {
                    var clientEnv = config.FindEnvironment(serverEnv.Name);

                    if (clientEnv == null)
                    {
                        continue;
                    }

                    foreach (var serverProcess in serverEnv.IndividualProcesses)
                    {
                        var clientProcess = clientEnv.FindProcess(serverProcess.Name);

                        if (clientProcess == null)
                        {
                            continue;
                        }

                        serverProcess.ExcludeFromStart = clientProcess.ExcludeFromStart;
                        serverProcess.ExcludeFromStop = clientProcess.ExcludeFromStop;
                    }

                    foreach (var serverGroup in serverEnv.Groups)
                    {
                        var clientGroup = clientEnv.FindGroup(serverGroup.Name);

                        if (clientGroup == null)
                        {
                            continue;
                        }

                        foreach (var serverProcess in serverGroup.Processes)
                        {
                            var clientProcess = clientGroup.FindProcess(serverProcess.Name);

                            if (clientProcess == null)
                            {
                                continue;
                            }

                            serverProcess.ExcludeFromStart = clientProcess.ExcludeFromStart;
                            serverProcess.ExcludeFromStop = clientProcess.ExcludeFromStop;
                        }
                    }
                }
            }

            return serverConfiguration;
        }

        private async Task ExclusionChanged()
        {
            await LocalStorage.SetItemAsync("config", configuration);
        }

        private IEnumerable<string> GetStartExcludedProcessesForEnvironment()
        {
            return configuration.FindEnvironment(EnvironmentName).IndividualProcesses
                .Where(proc => proc.ExcludeFromStart)
                .Select(proc => proc.Name);
        }

        private IEnumerable<string> GetStopExcludedProcessesForEnvironment()
        {
            return configuration.FindEnvironment(EnvironmentName).IndividualProcesses
                .Where(proc => proc.ExcludeFromStop)
                .Select(proc => proc.Name);
        }

        private IEnumerable<string> GetStartExcludedProcessesForGroup(string groupName)
        {
            return configuration.FindEnvironment(EnvironmentName).FindGroup(groupName).Processes
                .Where(proc => proc.ExcludeFromStart)
                .Select(proc => proc.Name);
        }

        private IEnumerable<string> GetStopExcludedProcessesForGroup(string groupName)
        {
            return configuration.FindEnvironment(EnvironmentName).FindGroup(groupName).Processes
                .Where(proc => proc.ExcludeFromStop)
                .Select(proc => proc.Name);
        }

        private void OnPageVisibilityChanged(bool isPageVisible)
        {
            this.isPageVisible = isPageVisible;
        }

        private void OnTabChanged(int tabIndex)
        {
            isCurrentTabRequiresPolling = tabIndex != 1;
        }
    }
}
