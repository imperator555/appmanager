﻿using Microsoft.JSInterop;

namespace AppManager.Client.Helpers
{
    public class VisibilityChangedHelper
    {
        public delegate void VisibilityChangedHandler(bool isVisible);
        public static event VisibilityChangedHandler VisibilityChanged;

        [JSInvokable]
        public static void SignalVisibilityChanged(bool isVisible)
        {
            VisibilityChanged?.Invoke(isVisible);
        }
    }
}
