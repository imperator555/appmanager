﻿using AppManager.Client.Model;
using System.Collections.Generic;
using System.Linq;

namespace AppManager.Client.Services
{
    public class MapperService : IMapperService
    {
        public Configuration MapConfiguration(AppManager.Shared.Configuration configuration)
        {
            return new Configuration
            {
                Environments = MapEnvironments(configuration.Environments)
            };
        }

        private IEnumerable<Model.Environment> MapEnvironments(IEnumerable<AppManager.Shared.Environment> environments)
        {
            return environments.Select(env => new Model.Environment()
            {
                Name = env.Name,
                Jobs = MapJobs(env.Jobs),
                IndividualProcesses = MapProcesses(env.IndividualProcesses),
                Groups = MapGroups(env.Groups)
            }).ToList();
        }

        private IEnumerable<Job> MapJobs(IEnumerable<AppManager.Shared.Job> jobs)
        {
            return jobs.Select(job => new Job()
            {
                Name = job.Name
            }).ToList();
        }

        private IEnumerable<Process> MapProcesses(IEnumerable<AppManager.Shared.Process> individualProcesses)
        {
            return individualProcesses.Select(proc => new Process()
            {
                Name = proc.Name,
                Health = proc.Health,
                Dependencies = new List<string>(proc.Dependencies),
                ExcludeFromStart = false,
                ExcludeFromStop = false
            }).ToList();
        }

        private IEnumerable<ProcessGroup> MapGroups(IEnumerable<AppManager.Shared.ProcessGroup> groups)
        {
            return groups.Select(group => new ProcessGroup()
            {
                Name = group.Name,
                Processes = MapProcesses(group.Processes)
            }).ToList();
        }
    }
}
