﻿using AppManager.Client.Model;

namespace AppManager.Client.Services
{
    public interface IMapperService
    {
        Configuration MapConfiguration(AppManager.Shared.Configuration configuration);
        
    }
}