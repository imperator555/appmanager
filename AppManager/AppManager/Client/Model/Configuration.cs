﻿using System.Collections.Generic;
using System.Linq;

namespace AppManager.Client.Model
{
    public class Configuration
    {
        public IEnumerable<Environment> Environments { get; set; }

        public Environment FindEnvironment(string name)
        {
            return Environments.Where(env => env.Name == name).FirstOrDefault();
        }
    }
}
