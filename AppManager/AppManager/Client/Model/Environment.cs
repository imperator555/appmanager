﻿using System.Collections.Generic;
using System.Linq;

namespace AppManager.Client.Model
{
    public class Environment
    {
        public string Name { get; set; }

        public IEnumerable<Process> IndividualProcesses { get; set; }

        public IEnumerable<Job> Jobs { get; set; }

        public IEnumerable<ProcessGroup> Groups { get; set; }

        public Process FindProcess(string name)
        {
            return IndividualProcesses.Where(p => p.Name == name).FirstOrDefault();
        }

        public ProcessGroup FindGroup(string name)
        {
            return Groups.Where(g => g.Name == name).FirstOrDefault();
        }
    }
}
