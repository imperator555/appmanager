﻿using AppManager.Shared;
using System.Collections.Generic;

namespace AppManager.Client.Model
{
    public class Process
    {
        public string Name { get; set; }

        public ProcessHealth Health { get; set; }

        public IEnumerable<string> Dependencies { get; set; }

        public bool ExcludeFromStart { get; set; }

        public bool ExcludeFromStop { get; set; }
    }
}
