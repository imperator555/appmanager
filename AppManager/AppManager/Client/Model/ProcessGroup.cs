﻿using System.Collections.Generic;
using System.Linq;

namespace AppManager.Client.Model
{
    public class ProcessGroup
    {
        public string Name { get; set; }
        public IEnumerable<Process> Processes { get; set; }

        public Process FindProcess(string name)
        {
            return Processes.Where(p => p.Name == name).FirstOrDefault();
        }
    }
}
