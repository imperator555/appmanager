﻿using AppManager.Server.Services;
using AppManager.Shared;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppManager.Server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProcessController : ControllerBase
    {
        private readonly IProcessService processService;
        private readonly IConfigurationService configurationService;

        public ProcessController(IProcessService processService, IConfigurationService configurationService)
        {
            this.processService = processService;
            this.configurationService = configurationService;
        }

        [HttpGet("health")]
        public async Task<Configuration> GetConfigurationHealth()
        {
            return await processService.GetConfigurationHealth();
        }

        [HttpGet("environments")]
        public IEnumerable<string> GetEnvironments()
        {
            return configurationService.LoadConfiguration().Environments.Select(env => env.Name);
        }

        [HttpGet("startProcess")]
        public void StartProcess(string env, string proc)
        {
            processService.StartProcess(env, proc);
        }

        [HttpGet("stopProcess")]
        public void StopProcess(string env, string proc)
        {
            processService.StopProcess(env, proc);
        }

        [HttpGet("startGroup")]
        public void StartGroup(string env, string group, string exclusions)
        {
            processService.StartGroup(env, group, SplitExclusions(exclusions));
        }

        [HttpGet("stopGroup")]
        public void StopGroup(string env, string group, string exclusions)
        {
            processService.StopGroup(env, group, SplitExclusions(exclusions));
        }

        [HttpGet("startEnvironment")]
        public void StartEnvironment(string env, string exclusions)
        {
            processService.StartEnvironment(env, SplitExclusions(exclusions));
        }

        [HttpGet("stopEnvironment")]
        public void StopEnvironment(string env, string exclusions)
        {
            processService.StopEnvironment(env, SplitExclusions(exclusions));
        }

        [HttpGet("runJob")]
        public void RunJob(string env, string job)
        {
            processService.RunJob(env, job);
        }

        private IEnumerable<string> SplitExclusions(string exclusions)
        {
            return exclusions != null ? exclusions.Split(',') : Array.Empty<string>();
        }
    }
}
