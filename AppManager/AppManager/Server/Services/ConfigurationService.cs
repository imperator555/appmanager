﻿using AppManager.Shared;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;

namespace AppManager.Server.Services
{
    public interface IConfigurationService
    {
        Configuration LoadConfiguration();
    }

    public class ConfigurationService : IConfigurationService
    {
        private readonly string configurationPath;

        private readonly ILogger<ConfigurationService> logger;

        public ConfigurationService(ILogger<ConfigurationService> logger, IOptions<AppConfiguration> appConfiguration)
        {
            this.logger = logger;
            configurationPath = appConfiguration.Value.ConfigurationFolder;
        }

        public Configuration LoadConfiguration()
        {
            var result = new List<Shared.Environment>();

            foreach (var envDirectoryPath in Directory.EnumerateDirectories(configurationPath))
            {
                var dirInfo = new DirectoryInfo(envDirectoryPath);

                if (!dirInfo.Name.StartsWith('.'))
                {
                    result.Add(LoadEnvironment(dirInfo));
                }
            }

            return new Configuration
            {
                Environments = result
            };
        }

        private Shared.Environment LoadEnvironment(DirectoryInfo environmentDirectoryInfo)
        {
            var environment = new Shared.Environment()
            {
                Name = environmentDirectoryInfo.Name
            };

            var processList = new List<Process>();
            var processesDirectory = Path.Combine(environmentDirectoryInfo.FullName, ".processes");

            foreach (var processPath in Directory.EnumerateDirectories(processesDirectory))
            {
                processList.Add(LoadProcess(new DirectoryInfo(processPath)));
            }

            environment.IndividualProcesses = processList;

            var groupList = new List<ProcessGroup>();
            var groupsDirectory = Path.Combine(environmentDirectoryInfo.FullName, ".groups");

            foreach (var groupConfig in Directory.EnumerateFiles(groupsDirectory))
            {
                groupList.Add(LoadGroup(new FileInfo(groupConfig), environment.IndividualProcesses));
            }

            environment.Groups = groupList;

            var jobList = new List<Job>();
            var jobsDirectory = Path.Combine(environmentDirectoryInfo.FullName, ".jobs");

            foreach (var jobConfig in Directory.EnumerateFiles(jobsDirectory))
            {
                jobList.Add(LoadJob(new FileInfo(jobConfig)));
            }

            environment.Jobs = jobList;

            var startupScripts = new List<Script>();
            var startupScriptsDirectory = Path.Combine(environmentDirectoryInfo.FullName, ".onstartup");

            foreach (var startupScript in Directory.EnumerateFiles(startupScriptsDirectory))
            {
                startupScripts.Add(new Script()
                {
                    Path = startupScript,
                    Type = ScriptType.PowerShell
                });
            }

            environment.StartupScripts = startupScripts;

            return environment;
        }

        private Process LoadProcess(DirectoryInfo processDirectoryInfo)
        {
            var processConfig = LoadProcessConfig(Path.Combine(processDirectoryInfo.FullName, "config.json"));

            return new Process()
            {
                Name = processDirectoryInfo.Name,
                HealthScript = new Script() 
                {
                    Path = Path.Combine(processDirectoryInfo.FullName, "health.ps1"),
                    Type = ScriptType.PowerShell
                },
                StartScript = new Script()
                {
                    Path = Path.Combine(processDirectoryInfo.FullName, "start.ps1"),
                    Type = ScriptType.PowerShell
                },
                StopScript = new Script()
                {
                    Path = Path.Combine(processDirectoryInfo.FullName, "stop.ps1"),
                    Type = ScriptType.PowerShell
                },
                Dependencies = processConfig.Dependencies,
                Health = ProcessHealth.Down
            };
        }

        private ProcessConfig LoadProcessConfig(string configPath)
        {
            if (File.Exists(configPath))
            {
                JsonSerializerOptions options = new JsonSerializerOptions
                {
                    PropertyNamingPolicy = JsonNamingPolicy.CamelCase
                };

                return JsonSerializer.Deserialize<ProcessConfig>(File.ReadAllText(configPath), options);
            } 
            else
            {
                return new ProcessConfig() 
                { 
                    Dependencies = Array.Empty<string>() 
                };
            }
        }

        private ProcessGroup LoadGroup(FileInfo groupConfigFileInfo, IEnumerable<Process> individualProcesses)
        {
            var processNames = File.ReadAllText(groupConfigFileInfo.FullName).Trim().Split("\n").Select(str => str.Trim());

            CheckIfGroupProcessesExists(groupConfigFileInfo.Name, processNames, individualProcesses);

            return new ProcessGroup()
            {
                Name = groupConfigFileInfo.Name,
                Processes = processNames.Select(name => individualProcesses.Where(proc => proc.Name == name).First())
            };
        }

        private void CheckIfGroupProcessesExists(string groupName, IEnumerable<string> groupProcessNames, IEnumerable<Process> individualProcesses)
        {
            var nonExistsentProcesses = groupProcessNames.Where(name => individualProcesses.All(knownProc => knownProc.Name != name));

            if (nonExistsentProcesses.Any())
            {
                foreach (var proc in nonExistsentProcesses) {
                    logger.LogError($"Non-Existent process ({proc}) referenced in group ({groupName})");
                }

                throw new Exception("Non-existent process referenced in a group.");
            }
        }

        private Job LoadJob(FileInfo jobFileInfo)
        {
            return new Job()
            {
                Name = jobFileInfo.Name,
                Script = new Script()
                {
                    Path = jobFileInfo.FullName,
                    Type = ScriptType.PowerShell
                }
            };
        }

        private class ProcessConfig
        {
            public IEnumerable<string> Dependencies { get; set; }
        }
    }
}
