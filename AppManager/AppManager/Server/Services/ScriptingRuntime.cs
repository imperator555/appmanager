﻿using AppManager.Server.Runners;
using AppManager.Shared;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AppManager.Server.Services
{
    public interface IScriptingRuntime
    {
        Task<string> RunScript(string key, Script script);
    }

    public class ScriptingRuntime : IScriptingRuntime, ISimpleDisposable
    {
        private readonly object syncRoot = new object();

        private readonly IServiceProvider serviceProvider;
        private readonly IConfigurationService configurationService;

        private readonly IDictionary<RunnerKey, IRunner> scriptRunners = new ConcurrentDictionary<RunnerKey, IRunner>();

        bool ISimpleDisposable.Disposed { get; set; }

        public ScriptingRuntime(IServiceProvider serviceProvider, IConfigurationService configurationService)
        {
            this.serviceProvider = serviceProvider;
            this.configurationService = configurationService;
        }

        public async Task<string> RunScript(string key, Script script)
        {
            var runnerKey = new RunnerKey(key, script.Type);
            var runner = FindOrCreateRunner(runnerKey);

            return await runner.RunScript(script.Path);
        }

        private IRunner FindOrCreateRunner(RunnerKey runnerKey)
        {
            if (!scriptRunners.ContainsKey(runnerKey))
            {
                lock (syncRoot)
                {
                    if (!scriptRunners.ContainsKey(runnerKey))
                    {
                        scriptRunners[runnerKey] = CreateNewRunner(runnerKey);
                    }
                }
            }

            return scriptRunners[runnerKey];
        }

        private IRunner CreateNewRunner(RunnerKey runnerKey)
        {
            switch (runnerKey.type)
            {
                case ScriptType.PowerShell:
                    var runner = GetInstance<PowershellRunner>();
                    runner.RunGlobalScripts(configurationService.LoadConfiguration().GetGlobalScriptsForEnvironment(runnerKey.key, ScriptType.PowerShell));

                    return runner;

                default:
                    throw new ArgumentException($"Unknown script type: {runnerKey.type}");
            }
        }

        private T GetInstance<T>() where T : class
        {
            var activatorFactory = ActivatorUtilities.CreateFactory(typeof(T), Array.Empty<Type>());
            return activatorFactory.Invoke(serviceProvider, Array.Empty<object>()) as T;
        }

        void ISimpleDisposable.OnDisposing()
        {
            foreach (var runner in scriptRunners.Values)
            {
                runner.Dispose();
            }
        }

        private struct RunnerKey
        {
            public readonly string key;
            public readonly ScriptType type;

            public RunnerKey(string key, ScriptType type)
            {
                this.key = key;
                this.type = type;
            }

            public override bool Equals(object other)
            {
                return other is RunnerKey key && Equals(key);
            }

            public bool Equals(RunnerKey other)
            {
                return this == other;
            }

            public override int GetHashCode()
            {
                return HashCode.Combine(key, type);
            }

            public static bool operator==(RunnerKey obj1, RunnerKey obj2)
            {
                if (obj1 == null || obj2 == null)
                {
                    return false;
                }

                if (ReferenceEquals(obj1, obj2))
                {
                    return true;
                }

                return obj1.key == obj2.key && obj1.type == obj2.type;
            }

            public static bool operator!=(RunnerKey obj1, RunnerKey obj2)
            {
                return !(obj1 == obj2);
            }
        }
    }
}
