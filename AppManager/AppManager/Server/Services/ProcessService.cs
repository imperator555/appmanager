﻿using AppManager.Shared;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace AppManager.Server.Services
{
    public interface IProcessService
    {
        Task<Configuration> GetConfigurationHealth();
        void StartProcess(string environemnt, string process);
        void StopProcess(string environment, string process);
        void StartGroup(string env, string group, IEnumerable<string> exclusions);
        void StopGroup(string env, string group, IEnumerable<string> exclusions);
        void StartEnvironment(string env, IEnumerable<string> exclusions);
        void StopEnvironment(string env, IEnumerable<string> exclusions);
        void RunJob(string env, string job);
    }

    public class ProcessService : IProcessService
    {
        private readonly ILogger<IProcessService> logger;

        private readonly IConfigurationService configurationService;
        private readonly IScriptingRuntime scriptingRuntime;

        public ProcessService(IConfigurationService configurationService, ILogger<IProcessService> logger, IScriptingRuntime scriptingRuntime)
        {
            this.configurationService = configurationService;
            this.logger = logger;
            this.scriptingRuntime = scriptingRuntime;
        }

        public async Task<Configuration> GetConfigurationHealth()
        {
            var configuration = configurationService.LoadConfiguration();
            var tasks = new List<Task>();

            foreach (var env in configuration.Environments)
            {
                foreach (var process in env.IndividualProcesses)
                {
                    tasks.Add(Task.Run(async () => process.Health = await GetProcessHealth(env.Name, process)));
                }
            }

            await Task.WhenAll(tasks);

            return configuration;
        }

        public void StartProcess(string environment, string process)
        {
            logger.LogInformation($"starting {environment}/{process}");
            var configuration = configurationService.LoadConfiguration();
            var proc = configuration.FindProcess(environment, process);
            scriptingRuntime.RunScript(environment, proc.StartScript);
        }

        public void StopProcess(string environment, string process)
        {
            var configuration = configurationService.LoadConfiguration();
            var proc = configuration.FindProcess(environment, process);
            scriptingRuntime.RunScript(environment, proc.StopScript);
        }

        public void StartGroup(string environment, string group, IEnumerable<string> exclusions)
        {
            var configuration = configurationService.LoadConfiguration();
            var processes = FilterExclusions(configuration.FindGroup(environment, group).Processes, exclusions);

            StartMultipleProcesses(environment, processes);
        }

        public void StopGroup(string environment, string group, IEnumerable<string> exclusions)
        {
            var configuration = configurationService.LoadConfiguration();
            var processes = FilterExclusions(configuration.FindGroup(environment, group).Processes, exclusions);

            StopMultipleProcesses(environment, processes);
        }

        public void StartEnvironment(string environment, IEnumerable<string> exclusions)
        {
            var configuration = configurationService.LoadConfiguration();
            var processes = FilterExclusions(configuration.FindEnvironment(environment).IndividualProcesses, exclusions);

            StartMultipleProcesses(environment, processes);
        }

        public void StopEnvironment(string environment, IEnumerable<string> exclusions)
        {
            var configuration = configurationService.LoadConfiguration();
            var processes = FilterExclusions(configuration.FindEnvironment(environment).IndividualProcesses, exclusions);

            StopMultipleProcesses(environment, processes);
        }

        public void RunJob(string environment, string jobName)
        {
            var configuration = configurationService.LoadConfiguration();
            var job = configuration.FindJob(environment, jobName);
            scriptingRuntime.RunScript(environment, job.Script);
        }

        private void StartMultipleProcesses(string environment, IEnumerable<Shared.Process> processes)
        {
            foreach (var process in OrderProcessesByDependencies(processes))
            {
                WaitForDelayOrProcessesUp(environment, 500, processes.Where(proc => process.Dependencies.Any(dep => proc.Name == dep)));
                StartProcess(environment, process.Name);
            }
        }

        private void StopMultipleProcesses(string environment, IEnumerable<Shared.Process> processes)
        {
            foreach (var process in OrderProcessesByDependencies(processes).Reverse())
            {
                StopProcess(environment, process.Name);
            }
        }

        private void WaitForDelayOrProcessesUp(string environment, int minimumDelay, IEnumerable<Shared.Process> processes)
        {
            Task.WaitAll(Task.Delay(minimumDelay), Task.WhenAll(processes.Select(proc => WaitUntilProcessIsUp(environment, proc, 30000))));
        }

        private async Task WaitUntilProcessIsUp(string environment, Shared.Process process, int timeoutInMs)
        {
            var watch = new Stopwatch();
            var isProcessUp = false;
            var isTimeout = false;
            
            while (!isProcessUp && !isTimeout)
            {
                isProcessUp = await CheckIfProcessIsUp(environment, process);
                isTimeout = watch.ElapsedMilliseconds >= timeoutInMs;
                await Task.Delay(1000);
            }

            watch.Stop();

            if (!isProcessUp && isTimeout)
            {
                throw new Exception($"Starting process({process.Name}) timed out");
            }
        }

        private async Task<bool> CheckIfProcessIsUp(string environment, Shared.Process process)
        {
            var health = await GetProcessHealth(environment, process);

            logger.LogInformation($"Dependency health check: {process.Name} is {health}");

            return health == ProcessHealth.Up;
        }

        private async Task<ProcessHealth> GetProcessHealth(string environment, Shared.Process process)
        {
            return ConvertToProcessHealth(await scriptingRuntime.RunScript(environment, process.HealthScript));
        }

        private ProcessHealth ConvertToProcessHealth(string healthString)
        {
            return healthString.Trim() switch
            {
                "UP" => ProcessHealth.Up,
                _ => ProcessHealth.Down
            };
        }

        private IEnumerable<Shared.Process> OrderProcessesByDependencies(IEnumerable<Shared.Process> processes)
        {
            var unorderedProcesses = new List<Shared.Process>(processes);
            var orderedProcesses = new List<Shared.Process>();

            // we are limiting the maximum number of iterations since we are using a naive "ordering algorithm"
            // that is unable to detect circles in the dependency graph
            int iterationsRemaining = 1000;

            while (unorderedProcesses.Any() && iterationsRemaining > 0)
            {
                iterationsRemaining--;

                for (int i = unorderedProcesses.Count-1; i >= 0; i--)
                {
                    var candidate = unorderedProcesses[i];
                    var hasDependency = false;

                    foreach (var dependency in candidate.Dependencies)
                    {
                        if (unorderedProcesses.Any(proc => proc.Name == dependency))
                        {
                            hasDependency = true;
                            break;
                        }
                    }

                    if (hasDependency)
                    {
                        continue;
                    }

                    orderedProcesses.Add(candidate);
                    unorderedProcesses.Remove(candidate);
                }
            }

            return orderedProcesses;
        }

        private IEnumerable<Shared.Process> FilterExclusions(IEnumerable<Shared.Process> processes, IEnumerable<string> exclusions)
        {
            return processes
                .Where(process => exclusions.All(name => name != process.Name))
                .ToList();
        }
    }
}
