﻿using AppManager.Shared;
using Microsoft.Extensions.Logging;
using Microsoft.PowerShell;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Text;
using System.Threading.Tasks;

namespace AppManager.Server.Runners
{
    public class PowershellRunner : IRunner
    {
        private readonly ILogger<PowershellRunner> logger;

        private readonly RunspacePool runspacePool;

        bool ISimpleDisposable.Disposed { get; set; }

        public PowershellRunner(ILogger<PowershellRunner> logger)
        {
            this.logger = logger;

            // Create a default initial session state and set the execution policy.
            InitialSessionState initialSessionState = InitialSessionState.CreateDefault();
            initialSessionState.ExecutionPolicy = ExecutionPolicy.Bypass;

            runspacePool = RunspaceFactory.CreateRunspacePool(initialSessionState);
            runspacePool.SetMaxRunspaces(System.Environment.ProcessorCount);
            runspacePool.Open();
        }

        public async Task<string> RunScript(string path)
        {
            var powershell = CreatePowershellCommand(path);

            var psResults = await InvokeCommandAsync(powershell, path);

            return BuildScriptOutput(psResults);
        }

        public void RunGlobalScripts(IEnumerable<string> scripts)
        {
            var tasks = scripts.Select(scriptPath =>
            {
                var powershell = CreatePowershellCommand(scriptPath, false);
                return InvokeCommandAsync(powershell, scriptPath);
            }).ToArray();

            Task.WaitAll(tasks);
        }

        private PowerShell CreatePowershellCommand(string path, bool localScope = true)
        {
            var powershell = PowerShell.Create();
            powershell.AddScript(path, localScope);
            powershell.RunspacePool = runspacePool;

            return powershell;
        }

        private string BuildScriptOutput(IEnumerable<PSObject> results)
        {
            StringBuilder stringBuilder = new StringBuilder();

            foreach (PSObject obj in results)
            {
                stringBuilder.AppendLine(obj.ToString());
            }

            return stringBuilder.ToString();
        }

        private Task<IEnumerable<PSObject>> InvokeCommandAsync(PowerShell powershell, string logLabel = "")
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            var taskSource = new TaskCompletionSource<IEnumerable<PSObject>>();

            powershell.BeginInvoke(
                new PSDataCollection<string>(),
                new PSInvocationSettings(),
                result =>
                {
                    stopWatch.Stop();
                    logger.LogInformation($"{logLabel} ran for {stopWatch.ElapsedMilliseconds}ms ");

                    LogError(powershell);
                    taskSource.SetResult(powershell.EndInvoke(result));
                },
                powershell);

            return taskSource.Task;
        }

        private void LogError(PowerShell powershell)
        {
            if (powershell.HadErrors && !IsPhantomError(powershell))
            {
                logger.LogError(powershell.InvocationStateInfo.Reason?.Message);

                foreach (var error in powershell.Streams.Error)
                {
                    logger.LogError(error.ToString());
                }
            }
        }

        private bool IsPhantomError(PowerShell powershell)
        {
            return powershell.InvocationStateInfo.State == PSInvocationState.Completed
                && powershell.InvocationStateInfo.Reason == null
                && powershell.Streams.Error.Count == 0;
        }

        void ISimpleDisposable.OnDisposing()
        {
            runspacePool.Close();
        }

        void IDisposable.Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
