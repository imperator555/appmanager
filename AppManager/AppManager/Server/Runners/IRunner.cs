﻿using AppManager.Shared;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AppManager.Server.Runners
{
    public interface IRunner : ISimpleDisposable
    {
        void RunGlobalScripts(IEnumerable<string> scripts);

        Task<string> RunScript(string script);
    }
}
